package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import ub.services.majales.dao.Meeting;

public interface MeetingRepository extends JpaRepository<Meeting, Long>{

	
}
