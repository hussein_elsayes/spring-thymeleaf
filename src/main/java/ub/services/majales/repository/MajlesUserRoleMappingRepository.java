package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ub.services.majales.dao.UserMajlesMapping;

public interface MajlesUserRoleMappingRepository extends JpaRepository<UserMajlesMapping, Long> {

}
