package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ub.services.majales.dao.LdapUser;

public interface LdapUserRepository extends JpaRepository<LdapUser, String> {

}
