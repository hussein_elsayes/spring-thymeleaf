package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ub.services.majales.dao.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
