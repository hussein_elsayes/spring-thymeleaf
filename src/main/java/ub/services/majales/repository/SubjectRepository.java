package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ub.services.majales.dao.Subject;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
