package ub.services.majales.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import ub.services.majales.dao.Majles;

public interface MajlesRepository extends JpaRepository<Majles, Long> {
	
	@Modifying
	@Transactional
	@Query(value = "DELETE FROM majales.user_majles_mapping where mapping_id=?1" , nativeQuery = true)
	public void deleteUserMapping(Long mapping_id);
	
}
