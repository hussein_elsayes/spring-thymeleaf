package ub.services.majales.dao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "majles_users")
public class UserMajlesMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MAPPING_ID")
	private long mappingId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MAJLES_ID")
	private Majles majles;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SAM_ACCOUNT")
	private LdapUser ldapUser;

	@Column(name = "SUPERVISOR")
	private Boolean isSupervisor;

	public long getMappingId() {
		return mappingId;
	}

	public void setMappingId(long mappingId) {
		this.mappingId = mappingId;
	}

	public Majles getMajles() {
		return majles;
	}

	public void setMajles(Majles majles) {
		this.majles = majles;
	}

	public LdapUser getLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(LdapUser ldapUser) {
		this.ldapUser = ldapUser;
	}

	public Boolean getIsSupervisor() {
		return isSupervisor;
	}

	public void setIsSupervisor(Boolean isSupervisor) {
		this.isSupervisor = isSupervisor;
	}


}
