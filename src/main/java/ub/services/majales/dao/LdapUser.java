package ub.services.majales.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class LdapUser {
	@Id
	@Column(name = "SAM_ACCOUNT")
	private String samAccount;
	@Column(name = "MOBILE_NO")
	private String mobileNo;
	@Column(name="ARABIC_NAME")
	private String arabicName;
	@OneToMany(mappedBy= "ldapUser" ,cascade= CascadeType.ALL)
	List<UserMajlesMapping> mappings;

	//transient : used only to transfer temp data about the user
	@Transient
	private Boolean isSupervisorTemp;
	
	public String getSamAccount() {
		return samAccount;
	}
	public void setSamAccount(String samAccount) {
		this.samAccount = samAccount;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public String getArabicName() {
		return arabicName;
	}
	public void setArabicName(String arabicName) {
		this.arabicName = arabicName;
	}
	public List<UserMajlesMapping> getMappings() {
		return mappings;
	}
	public void setMappings(List<UserMajlesMapping> mappings) {
		this.mappings = mappings;
	}
	public Boolean getIsSupervisorTemp() {
		return isSupervisorTemp;
	}
	public void setIsSupervisorTemp(Boolean isSupervisorTemp) {
		this.isSupervisorTemp = isSupervisorTemp;
	}
	
	
}
