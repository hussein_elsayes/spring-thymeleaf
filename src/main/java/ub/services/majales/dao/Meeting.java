package ub.services.majales.dao;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Meeting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="MEETING_ID")
	private Long id;
	@Column(name = "MEETING_NAME")
	private String name;
	@Column(name = "MEETING_DESC")
	private String description;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MAJLES_ID")
	private Majles majles;
	@ManyToMany
	private List<LdapUser> users;
	@OneToMany(mappedBy = "meeting", cascade = CascadeType.ALL)
	List<Subject> subjects;
	
	
	

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Majles getMajles() {
		return majles;
	}

	public void setMajles(Majles majles) {
		this.majles = majles;
	}
	
	public List<LdapUser> getUsers() {
		return users;
	}

	public void setUsers(List<LdapUser> users) {
		this.users = users;
	}

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	
	
}
