package ub.services.majales.dao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Subject {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	@Column(name = "SUBJECT_ID")
	private Long id;
	@Column(name = "SUBJECT_NAME")
	private String name;
	@Column(name = "SUBJECT_DESC")
	private String description;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MEETING_ID")
	private Meeting meeting;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Meeting getMeeting() {
		return meeting;
	}
	public void setMeeting(Meeting meeting) {
		this.meeting = meeting;
	}
	
	
}
