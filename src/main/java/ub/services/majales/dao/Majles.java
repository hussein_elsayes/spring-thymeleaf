package ub.services.majales.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MAJLES")
public class Majles {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MAJLES_ID")
	private long id;

	@Column(name = "MAJLES_NAME")
	private String name;

	@Column(name = "MAJLES_DESC")
	private String description;
	
	@OneToMany(mappedBy = "majles", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<UserMajlesMapping> userRoleMappings;

	@OneToMany(mappedBy = "majles" , cascade = CascadeType.ALL)
	private List<Meeting> meetings;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<UserMajlesMapping> getUserRoleMappings() {
		return userRoleMappings;
	}
	
	public void setUserRoleMappings(List<UserMajlesMapping> mappings) {
		this.userRoleMappings = mappings;
	}

	public void addUserRoleMapping(UserMajlesMapping userRoleMapping) {
		if(this.userRoleMappings == null)
		{
			this.userRoleMappings = new ArrayList<UserMajlesMapping>();
		}
		
		this.userRoleMappings.add(userRoleMapping);
	}

	public List<Meeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}
	
	

}
