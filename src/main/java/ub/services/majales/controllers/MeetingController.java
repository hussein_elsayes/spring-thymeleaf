package ub.services.majales.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ub.services.majales.dao.LdapUser;
import ub.services.majales.dao.Meeting;
import ub.services.majales.dao.Subject;
import ub.services.majales.dao.UserMajlesMapping;
import ub.services.majales.service.MajalesService;

@Controller
@RequestMapping("/meetings")
public class MeetingController {
	private MajalesService majalesService;

	MeetingController(MajalesService majalesService) {
		this.majalesService = majalesService;
	}

	@GetMapping("/{m}")
	public ModelAndView getMeetingDetails(@PathVariable("m") Long meetingId) {
		ModelAndView modelAndView = new ModelAndView();
		Meeting meeting = majalesService.getMeeting(meetingId);

		// (add user modal)get majles users except the one which is supervisor
		Stream<UserMajlesMapping> stream = meeting.getMajles().getUserRoleMappings().stream()
				.filter(mapping -> !(mapping.getIsSupervisor()));
		List<UserMajlesMapping> mappings = Arrays.asList(stream.toArray(UserMajlesMapping[]::new));
		List<LdapUser> majlesUsers = MajalesServiceUtility.MajlesUserMappingsToLdapUsers(mappings);
		modelAndView.addObject("filteredMajlesUsers", majlesUsers);
		// get meeting
		modelAndView.addObject("meeting", meeting);
		// get meeting subjects
		modelAndView.addObject("subjects", meeting.getSubjects());
		// (right users list) get meeting users injecting the privileges from the majles
		// mapppings
		List<LdapUser> meetingUsers = meeting.getUsers();
		List<LdapUser> userPrivilegesList = new ArrayList<LdapUser>();
		meeting.getMajles().getUserRoleMappings().forEach(mapping -> {
			LdapUser tempUser = mapping.getLdapUser();
			if (meetingUsers.contains(tempUser)) {
				tempUser.setIsSupervisorTemp(mapping.getIsSupervisor());
				userPrivilegesList.add(tempUser);
			}
		});
		modelAndView.addObject("meetingUsers", userPrivilegesList);
		// add empty subject object to insert new form
		modelAndView.addObject("subject", new Subject());
		modelAndView.setViewName("majales/meeting_details");
		return modelAndView;
	}

	@GetMapping("/{m}/deleteUser/{samAccount}")
	public ModelAndView removeMajlesUser(@PathVariable("m") Long meetingId,
			@PathVariable("samAccount") String samAccount) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/meetings/" + meetingId);
		majalesService.removeMeetingUser(meetingId, samAccount);
		return modelAndView;
	}

	@PostMapping("/addSubject")
	public ModelAndView addMeetingSubject(@ModelAttribute("subject") @Valid Subject subject,
			@RequestParam("meetingId") Long meetingId) {
		ModelAndView modelAndView = new ModelAndView();
		majalesService.addSubject(subject, meetingId);
		modelAndView.setViewName("redirect:/meetings/" + meetingId);
		return modelAndView;
	}
}
