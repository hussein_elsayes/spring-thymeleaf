package ub.services.majales.controllers;

import java.util.ArrayList;
import java.util.List;

import ub.services.majales.dao.LdapUser;
import ub.services.majales.dao.UserMajlesMapping;

public class MajalesServiceUtility {

	public static List<LdapUser> MajlesUserMappingsToLdapUsers(List<UserMajlesMapping> mappings)
	{
		List<LdapUser> users = new ArrayList<LdapUser>();
		if (mappings != null) {
			for (UserMajlesMapping mapping : mappings) {
				LdapUser tempUser = mapping.getLdapUser();
				tempUser.setIsSupervisorTemp(mapping.getIsSupervisor());  
				users.add(tempUser);
			}
		}
		return users;
	}
}
