package ub.services.majales.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ub.services.majales.dao.Document;
import ub.services.majales.dao.Subject;
import ub.services.majales.service.MajalesService;

@Controller
@RequestMapping("/subjects")
public class SubjectController {
	
	private MajalesService majalesService;

	public SubjectController(MajalesService majalesService) {
		this.majalesService = majalesService;
	}
	
	public ModelAndView addDocument(Document doc, Long subjectId)
	{
		ModelAndView modelAndView = new ModelAndView();
		Subject subject = majalesService.getSubject(subjectId);
		Long meetingId = subject.getMeeting().getId();
		modelAndView.setViewName("redirect:/meetings/" + meetingId);
		return modelAndView;
	}
}
