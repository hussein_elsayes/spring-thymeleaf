package ub.services.majales.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ub.services.majales.dao.LdapUser;
import ub.services.majales.dao.Majles;
import ub.services.majales.dao.Meeting;
import ub.services.majales.dao.UserMajlesMapping;
import ub.services.majales.service.MajalesService;

@Controller
@RequestMapping("/majales")
public class MajlesController {

	private MajalesService majalesService;

	public MajlesController(MajalesService majalesService) {
		this.majalesService = majalesService;
	}

	@GetMapping("/")
	public String showAddMajlesForm(Model model) {
		List<Majles> majales = majalesService.getAllMajales();
		model.addAttribute("majales", majales);
		model.addAttribute("majles", new Majles());
		return "majales/majales";
	}

	@PostMapping("/addMajles")
	public ModelAndView addMajles(@Valid Majles majles, Errors errors, Model model) {
		ModelAndView modelAndView = new ModelAndView();
		if (errors.hasErrors()) {
			modelAndView.setViewName("majales/add_majles");
			return modelAndView;
		} else {
			majalesService.addMajles(majles);
			modelAndView.setViewName("redirect:/majales/" + majles.getId());
			return modelAndView;
		}
	}

	@GetMapping("/{m}")
	public ModelAndView showMajlesDetailsForm(@PathVariable("m") Long majlesId) {
		ModelAndView modelAndView = new ModelAndView();
		Majles majles = majalesService.getMajles(majlesId);
		List<LdapUser> users = majalesService.getMajlesUsers(majlesId);
		modelAndView.addObject("meeting", new Meeting());
		modelAndView.addObject("meetings", majles.getMeetings());
		modelAndView.addObject("users", users);
		modelAndView.addObject("majles", majles);
		modelAndView.setViewName("majales/majles_details");
		return modelAndView;
	}

	@PostMapping("/addMajlesUser")
	public ModelAndView addMajlesUsers(@RequestParam("sam_account") String samAccount,
			@RequestParam("supervisor") Boolean supervisor, @RequestParam("majles_id") Long majlesId) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/majales/" + majlesId);
		try {
			majalesService.addMajlesUserMappings(majlesId, samAccount, supervisor);
			modelAndView.addObject("successMsg", "User Created Successfully");
		} catch (NoSuchElementException ex) {
			modelAndView.addObject("errorMsg", "No Such User Exists !!");
		}
		return modelAndView;
	}
	
	@GetMapping("/{m}/deleteUser/{samAccount}")
	public ModelAndView removeMajlesUser(@PathVariable("m") Long majlesId, @PathVariable("samAccount") String samAccount)
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/majales/" + majlesId);
		majalesService.removeMajlesUser(majlesId, samAccount);
		return modelAndView;
	}
	
	@PostMapping("/{m}/AddMeeting")
	public ModelAndView getTestTemplate(@PathVariable("m") Long majlesId, @RequestParam("name") String meetingName,
			@RequestParam("description") String meetingDescription, @RequestParam("meeting_users") String meetingUsers)
	{
		ModelAndView modelAndView = new ModelAndView();
		
		//ADD USERS TO MEETING
		String[] tempUsers = meetingUsers.split(",");
		List<LdapUser> ldapUsers = new ArrayList<LdapUser>();
		for(String user: tempUsers)
		{
			ldapUsers.add(majalesService.getLdapUser(user));
		}
		
		//SAVE MEETING OBJECT
		Meeting meeting = new Meeting();
		meeting.setName(meetingName);
		meeting.setDescription(meetingDescription);
		meeting.setUsers(ldapUsers);
		majalesService.addMeeting(meeting, majlesId);
		
		//BAKE MODEL ATTRIBUTES
		Majles majles = majalesService.getMajles(majlesId);
		List<LdapUser> users = majalesService.getMajlesUsers(majlesId);
		modelAndView.addObject("meeting", new Meeting());
		modelAndView.addObject("meetings", majles.getMeetings());
		modelAndView.addObject("users", users);
		modelAndView.addObject("majles", majles);
		modelAndView.setViewName("redirect:/majales/" + majlesId);
		return modelAndView;
	}
}
