package ub.services.majales.service;

import java.util.List;

import ub.services.majales.dao.LdapUser;
import ub.services.majales.dao.Majles;
import ub.services.majales.dao.Meeting;
import ub.services.majales.dao.Role;
import ub.services.majales.dao.Subject;

public interface MajalesService {
	//USERS
	public LdapUser getLdapUser(String samAccount);
	
	//MAJLES
	public Majles getMajles(Long majlesId);
	public List<Majles> getAllMajales();
	public List<LdapUser> getMajlesUsers(Long majlesId);
	public Majles addMajles(Majles majles);
	public void addMajlesUserMappings(Long majlesId, String user,Boolean supervisor);
	public void removeMajlesUser(long majlesId, String samAccount);
	public void notifyMajlesUsers(long majlesId);
	public List<Role> getMajlesUserRoles();
	
	//MEETING
	public Meeting getMeeting(Long meetingId);
	public Meeting addMeeting(Meeting meeting, Long majlesId);
	public void addMeetingUsers(List<LdapUser> users);
	public List<LdapUser> getMeetingUsers(Long meetingId);
	public void removeMeetingUser(Long meetingId, String samAccount);
	public void notifyMeetingUsers(Long meetingId);
	
	//SUBJECT
	public void addSubject(Subject subject,Long meetingId);
	public Subject getSubject(Long subjectId);
	
	
}
