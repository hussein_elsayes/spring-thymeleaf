package ub.services.majales.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import ub.services.majales.controllers.MajalesServiceUtility;
import ub.services.majales.dao.LdapUser;
import ub.services.majales.dao.Majles;
import ub.services.majales.dao.Meeting;
import ub.services.majales.dao.Role;
import ub.services.majales.dao.Subject;
import ub.services.majales.dao.UserMajlesMapping;
import ub.services.majales.repository.LdapUserRepository;
import ub.services.majales.repository.MajlesRepository;
import ub.services.majales.repository.MeetingRepository;
import ub.services.majales.repository.RoleRepository;
import ub.services.majales.repository.SubjectRepository;
import ub.services.majales.service.MajalesService;

@Service
public class MajalesServiceImpl implements MajalesService {

	private MajlesRepository majlesRepo;
	private RoleRepository roleRepo;
	private LdapUserRepository userRepo;
	private MeetingRepository meetingRepo;
	private SubjectRepository subjectRepo;

	public MajalesServiceImpl(MajlesRepository repository, RoleRepository roleRepo, LdapUserRepository userRepo,
			MeetingRepository meetingRepo, SubjectRepository subjectRepo) {
		this.majlesRepo = repository;
		this.roleRepo = roleRepo;
		this.userRepo = userRepo;
		this.meetingRepo = meetingRepo;
		this.subjectRepo = subjectRepo;
	}

	@Override
	public Majles getMajles(Long majlesId) {
		// TODO Auto-generated method stub
		return majlesRepo.findById(majlesId).get();
	}

	@Override
	public List<Majles> getAllMajales() {
		return majlesRepo.findAll();
	}

	@Override
	public List<LdapUser> getMajlesUsers(Long majlesId) {
		Majles majles = majlesRepo.findById(majlesId).get();
		List<UserMajlesMapping> mappings = majles.getUserRoleMappings();
		List<LdapUser> users = MajalesServiceUtility.MajlesUserMappingsToLdapUsers(mappings);
		return users;
	}

	@Override
	public Majles addMajles(Majles majles) {
		System.out.println("Majles Name : " + majles.getName());

		// --------------------------------- DUMP DATA
		// ----------------------------------------

		// create 2 dump roles
		Role role = new Role();
		role.setRoleName("Participant");
		roleRepo.save(role);
		Role role2 = new Role();
		role2.setRoleName("Supervisor");
		roleRepo.save(role2);

		// create 2 dump users
		LdapUser user1 = new LdapUser();
		user1.setSamAccount("hussein");
		user1.setMobileNo("966533800945");
		user1.setArabicName("حسين امام السايس");
		userRepo.save(user1);
		LdapUser user2 = new LdapUser();
		user2.setSamAccount("emam");
		user2.setMobileNo("966533800946");
		user2.setArabicName("امام حسين السايس");
		userRepo.save(user2);
		// ---------------------------------------------------------------------------------------

		return majlesRepo.save(majles);
	}

	@Override
	public void addMajlesUserMappings(Long majlesId, String user, Boolean supervisor) {
		Majles majlesObj = majlesRepo.findById(majlesId).get();
		UserMajlesMapping mapping = new UserMajlesMapping();
		mapping.setLdapUser(userRepo.findById(user).get());
		mapping.setIsSupervisor(supervisor);
		mapping.setMajles(majlesObj);
		majlesObj.addUserRoleMapping(mapping);
		majlesRepo.save(majlesObj);
	}

	@Override
	public void removeMajlesUser(long majlesId, String samAccount) {
		Majles majles = majlesRepo.findById(majlesId).get();
		List<UserMajlesMapping> mappings = majles.getUserRoleMappings();

		for (int i = 0; i < mappings.size(); i++) {
			UserMajlesMapping mapping = mappings.get(i);
			if (mapping.getMajles().getId() == majlesId
					&& mapping.getLdapUser().getSamAccount().equalsIgnoreCase(samAccount)) {
				majlesRepo.deleteUserMapping(mapping.getMappingId());
			}
		}

	}

	@Override
	public LdapUser getLdapUser(String samAccount) {
		return userRepo.findById(samAccount).get();
	}

	@Override
	public void notifyMajlesUsers(long majlesId) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Role> getMajlesUserRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Meeting getMeeting(Long meetingId) {
		return meetingRepo.findById(meetingId).get();
	}

	@Override
	public Meeting addMeeting(Meeting meeting, Long majlesId) {
		meeting.setMajles(majlesRepo.findById(majlesId).get());
		meetingRepo.save(meeting);
		return null;
	}

	@Override
	public void addMeetingUsers(List<LdapUser> users) {

	}

	@Override
	public List<LdapUser> getMeetingUsers(Long meetingId) {
		Meeting meeting = meetingRepo.findById(meetingId).get();
		List<LdapUser> users = meeting.getUsers();
		return users;
	}

	@Override
	public void removeMeetingUser(Long meetingId, String samAccount) {
		Meeting meeting = meetingRepo.findById(meetingId).get();
		Stream<LdapUser> ldapUsersStream = meeting.getUsers().stream()
				.filter(user -> user.getSamAccount().equals(samAccount));
		List<LdapUser> newList = Arrays.asList(ldapUsersStream.toArray(LdapUser[]::new));
		meeting.setUsers(newList);
		meetingRepo.save(meeting);
	}

	@Override
	public void notifyMeetingUsers(Long meetingId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addSubject(Subject subject, Long meetingId) {
		//get meeting by meeting ID
		Meeting meeting = meetingRepo.findById(meetingId).get();
		subject.setMeeting(meeting);
		subjectRepo.save(subject);
	}

	@Override
	public Subject getSubject(Long subjectId) {
		// TODO Auto-generated method stub
		return subjectRepo.findById(subjectId).get();
	}

}
