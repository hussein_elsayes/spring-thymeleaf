package ub.services.majales.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import ub.services.majales.service.FileService;

public class FileServiceImpl implements FileService {
	@Value("${file.upload-dir}")
	String uploadDirectory;
	
	
	public FileServiceImpl() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public void storeFile(MultipartFile file) throws IOException {
		//get normalized filename
		Path uploadPath = Paths.get(file.getOriginalFilename()).toAbsolutePath().normalize();
		//create directory
		Files.createDirectories(uploadPath);
		// clean file Name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		//get the path of the target file
		Path targetFilePath = uploadPath.resolve(fileName);
		//save the file and overwrite existing
		Files.copy(file.getInputStream(), targetFilePath, StandardCopyOption.REPLACE_EXISTING);
	}

	@Override
	public MultipartFile getFile(String fileName) {
		// TODO Auto-generated method stub
		return null;
	}

}
